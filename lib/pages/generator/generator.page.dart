import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../app.state.dart';
import 'big-card.view.dart';
import 'favorite.button.dart';

class GeneratorPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var appState = context.watch<MyAppState>();
    var currentPair = appState.current;

    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          BigCard(pair: currentPair),
          SizedBox(height: 10),
          Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              FavoriteButton(
                isFavorite: appState.favorites.contains(currentPair),
                favoriteToggledCallback: appState.toggleFavorite,
              ),
              SizedBox(width: 10),
              ElevatedButton(
                onPressed: () {
                  appState.getNext();
                },
                child: Text('Next'),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
