import 'package:flutter/material.dart';

class FavoriteButton extends StatelessWidget {
  final bool isFavorite;
  final Function favoriteToggledCallback;

  const FavoriteButton(
      {super.key,
      required this.isFavorite,
      required this.favoriteToggledCallback});

  Icon getIcon() => Icon(isFavorite ? Icons.favorite : Icons.favorite_border);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton.icon(
      onPressed: () => favoriteToggledCallback(),
      icon: getIcon(),
      label: Text('Like'),
    );
  }
}
