import 'package:english_words/english_words.dart';
import 'package:flutter/material.dart';
import 'favorites-list-item.view.dart';
import 'typedefs.dart';

class FavouritesPage extends StatelessWidget {
  final List<WordPair> favorites;
  final RemoveFavoriteCallback removeClickedCallback;
  const FavouritesPage(
      {super.key,
      required this.favorites,
      required this.removeClickedCallback});

  @override
  Widget build(BuildContext context) {
    if (favorites.isEmpty) {
      return Center(
        child: Text('No favorites yet.'),
      );
    }

    return ListView(
      children: [
        Padding(
          padding: const EdgeInsets.all(20),
          child: Text('You have '
              '${favorites.length} favorites:'),
        ),
        for (var pair in favorites)
          FavoritesListItem(
            wordPair: pair,
            removeClickedCallback: removeClickedCallback,
          ),
      ],
    );
  }
}
