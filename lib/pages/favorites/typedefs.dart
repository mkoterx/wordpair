import 'package:english_words/english_words.dart';

typedef RemoveFavoriteCallback = Function(WordPair pair);
