import 'package:english_words/english_words.dart';
import 'package:flutter/material.dart';

import 'typedefs.dart';

class FavoritesListItem extends StatelessWidget {
  final WordPair wordPair;
  final RemoveFavoriteCallback removeClickedCallback;
  const FavoritesListItem(
      {super.key, required this.wordPair, required this.removeClickedCallback});

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Icon(Icons.favorite),
      title: Row(
        children: [
          Text(wordPair.asLowerCase),
          SizedBox(
            width: 10,
          ),
          ElevatedButton.icon(
            onPressed: () {
              removeClickedCallback(wordPair);
            },
            style: ButtonStyle(
              foregroundColor: MaterialStateProperty.all<Color>(Colors.red),
            ),
            icon: Icon(
              Icons.delete,
              color: Colors.red,
            ),
            label: Text("Delete"),
          )
        ],
      ),
    );
  }
}
